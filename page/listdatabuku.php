<?php
    if (isset($_POST['btn_insert'])){
        $pinjam = $_SESSION['id_peminjaman'];
        if ($pinjam != ""){
            $id_buku = $_POST['btn_insert'];
            $today = date("Y-m-d");
            $today7 = date("Y-m-d", strtotime($today . "+7 day"));
            $username = $_SESSION['username'];

            // Validasi Buku Sama
            $a = $con->query("SELECT * FROM tb_detailpeminjaman WHERE id_peminjaman = '$pinjam' AND id_buku = '$id_buku'");
            if ($a -> num_rows > 0){
                $_SESSION['message'] = '<div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Buku Sudah dipinjam
                </div>';
            }else{
                // Validasi QTY
                $r = $con->query("SELECT * FROM tb_peminjaman WHERE id_peminjaman = '$pinjam'");
                if ($r -> num_rows > 0){
                    while ($rr = $r->fetch_array()){
                        $qty = $rr['qty'];
                    }
                }
                $qtynew = $qty + 1;
                
                if($qty > 2) {
                    $_SESSION['message'] = '<div class="alert bg-red alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Qty max
                    </div>';
                }else{
                    $con->query("UPDATE tb_peminjaman SET qty = '$qtynew' WHERE id_peminjaman='$pinjam'");
                    $con->query("INSERT INTO tb_detailpeminjaman VALUES ('$pinjam', '$id_buku', 'USER001', '$today', '$today7', '1')");
                    echo "<script>window.location='index.php?page=home'</script>";
                }
            }
        } else {
            $id_buku = $_POST['btn_insert'];
            $today = date("Y-m-d");
            $today7 = date("Y-m-d", strtotime($today . "+7 day"));
            $username = $_SESSION['username'];

            $carikode = $con->query("SELECT MAX(id_peminjaman) FROM tb_peminjaman");
            $datakode = mysqli_fetch_array($carikode);
            if ($datakode) {
                $nilaikode = substr($datakode[0], 5);
                $kode = (int) $nilaikode;
                $kode = $kode + 1;
                $kode_otomatis = "PNJAM".str_pad($kode, 3, "0", STR_PAD_LEFT);
            } else {
                $kode_otomatis = "PNJAM001";
            }

            $con->query("INSERT INTO tb_peminjaman VALUES ('$kode_otomatis', '$username', '1', 'PROSES', '1')");
            $con->query("INSERT INTO tb_detailpeminjaman VALUES ('$kode_otomatis', '$id_buku', 'USER001', '$today', '$today7', '1')");
            echo "<script>window.location='index.php?page=home'</script>";
        }
    }
?> 

<section class="content">
    <!-- Default Media -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php  isset($_SESSION['message']) ? $e=$_SESSION['message'] : $e=""; echo $e; unset($_SESSION['message']);?>
            <div class="card">
                <div class="header">
                    <h2>
                        List Data Buku
                        <small>Pilih Buku Yang Ingin Di Pinjam.</small>
                    </h2>
                </div>
                <div class="body">
                <?php
                if (isset($_GET['judul_buku'])) {
                    $judul_buku = $_GET['judul_buku'];
                    $r = $con->query("SELECT tb_buku.id_buku, tb_buku.judul_buku, tb_pengarang.nama_pengarang, tb_penerbit.nama_penerbit, tb_kategori.nama_kategori, tb_buku.jumlah_tersedia, tb_buku.nomor_rak, tb_buku.isbn FROM tb_penerbit INNER JOIN (tb_pengarang INNER JOIN (tb_kategori INNER JOIN tb_buku ON tb_kategori.id_kategori = tb_buku.id_kategori) ON tb_pengarang.id_pengarang = tb_buku.id_pengarang) ON tb_penerbit.id_penerbit = tb_buku.id_penerbit WHERE tb_buku.judul_buku LIKE '%$judul_buku%' ORDER BY tb_buku.judul_buku ASC");
                    while ($rr = $r->fetch_array()) {
                ?>
                    <div class="media">
                        <div class="media-body">
                        <form action="" method="POST">
                            <button type="submit" name="btn_insert" value="<?php echo $rr['id_buku']; ?>" class="btn <?php if($rr['jumlah_tersedia'] != '0') { echo 'bg-blue'; } else { echo 'bg-red'; } ?> btn-circle waves-effect waves-circle waves-light waves-float pull-right" <?php if($rr['jumlah_tersedia'] == '0') { echo 'disabled onclick="return false;"'; } ?>>
                                <i class="material-icons"><?php if($rr['jumlah_tersedia'] != '0') { echo 'add'; } else { echo 'block'; } ?></i>
                            </button>
                        </form>
                            <h4 class="media-heading"><?php echo $rr['judul_buku'];?></h4> Nomor Rak <?php echo $rr['nomor_rak'];?> <br> <?php if($rr['jumlah_tersedia'] != '0') { echo '<span class="badge bg-blue">Available</span>'; } else { echo '<span class="badge bg-red">Not Available</span>'; } ?>
                        </div>
                    </div>
                    <?php
                    }
                }else{
                    if (isset($_GET['id_pengarang'])) {
                        $pengarang = $_GET['id_pengarang'];
                    }else{
                        $pengarang = "";
                    }
    
                    if (isset($_GET['id_penerbit'])) {
                        $penerbit = $_GET['id_penerbit'];
                    }else{
                        $penerbit = "";
                    }
                    if (isset($_GET['id_kategori'])) {
                        $kategori = $_GET['id_kategori'];
                    }else{
                        $kategori = "";
                    }

                    $r = $con->query("SELECT tb_buku.id_buku, tb_buku.judul_buku, tb_pengarang.nama_pengarang, tb_penerbit.nama_penerbit, tb_kategori.nama_kategori, tb_buku.jumlah_tersedia, tb_buku.nomor_rak, tb_buku.isbn FROM tb_penerbit INNER JOIN (tb_pengarang INNER JOIN (tb_kategori INNER JOIN tb_buku ON tb_kategori.id_kategori = tb_buku.id_kategori) ON tb_pengarang.id_pengarang = tb_buku.id_pengarang) ON tb_penerbit.id_penerbit = tb_buku.id_penerbit WHERE tb_buku.id_pengarang = '$pengarang' OR tb_buku.id_penerbit = '$penerbit' OR tb_buku.id_kategori = '$kategori' ORDER BY tb_buku.judul_buku ASC");
                    while ($rr = $r->fetch_array()) {
                    ?>
                        <div class="media">
                            <div class="media-body">
                            <form action="" method="POST">
                                <button type="submit" name="btn_insert" value="<?php echo $rr['id_buku']; ?>" class="btn <?php if($rr['jumlah_tersedia'] != '0') { echo 'bg-blue'; } else { echo 'bg-red'; } ?> btn-circle waves-effect waves-circle waves-light waves-float pull-right" <?php if($rr['jumlah_tersedia'] == '0') { echo 'disabled onclick="return false;"'; } ?>>
                                    <i class="material-icons"><?php if($rr['jumlah_tersedia'] != '0') { echo 'add'; } else { echo 'block'; } ?></i>
                                </button>
                            </form>
                                <h4 class="media-heading"><?php echo $rr['judul_buku'];?></h4> Nomor Rak <?php echo $rr['nomor_rak'];?> <br> <?php if($rr['jumlah_tersedia'] != '0') { echo '<span class="badge bg-blue">Available</span>'; } else { echo '<span class="badge bg-red">Not Available</span>'; } ?>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Default Media -->
</section>