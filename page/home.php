<?php
    // // Hitung qty checkout
    // $result = $con->query("SELECT COUNT(id_menu) FROM orderdetails_temp");
    // $row = $result->fetch_row();
    // $total_menu = $row[0];

    // // Hitung menu available
    // $result = $con->query("SELECT COUNT(*) FROM menus WHERE status_menu='1'");
    // $row = $result->fetch_row();
    // $menu = $row[0];

    // // Hitung table available
    // $result = $con->query("SELECT COUNT(*) FROM tables WHERE status='0'");
    // $row = $result->fetch_row();
    // $table = $row[0];

    // $today = date("Y-m-d");
    // $result = $con->query("SELECT COUNT(*) FROM orders WHERE date_order='$today'");
    // $row = $result->fetch_row();
    // $ordertoday = $row[0];
?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>List Data Buku yang di Pinjam</h2>
        </div>
        <div class="card">
            <div class="body">
            <?php
            $s = $con->query("SELECT * FROM tb_peminjaman WHERE id_anggota = '$user' AND status_aktif = '1'");
            if ($s -> num_rows > 0){
                while ($ss = $s->fetch_array()){
                    $_SESSION['id_peminjaman'] = $ss['id_peminjaman'];
                }
            }else{
                $_SESSION['id_peminjaman'] = "";
            }
            
            $pinjam = $_SESSION['id_peminjaman'];

            if($pinjam != ""){
                $r = $con->query("SELECT tb_detailpeminjaman.id_buku, tb_buku.judul_buku, tb_pengarang.nama_pengarang, tb_penerbit.nama_penerbit FROM tb_penerbit INNER JOIN (tb_pengarang INNER JOIN (tb_buku INNER JOIN tb_detailpeminjaman ON tb_buku.id_buku = tb_detailpeminjaman.id_buku) ON tb_pengarang.id_pengarang = tb_buku.id_pengarang) ON tb_penerbit.id_penerbit = tb_buku.id_penerbit WHERE id_peminjaman='$pinjam' AND status_aktif='1'");
                while ($rr = $r->fetch_array()) {

                ?>
                    <div class="media">
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $rr['judul_buku'];?></h4><?php echo $rr['nama_pengarang'];?> <br> <?php echo $rr['nama_penerbit'];?>
                        </div>
                    </div>
                <?php
                    }
                ?>
            <?php
            }else{
                echo '<div class="alert bg-pink alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Tidak ada pinjaman
                </div>';
            }
            ?>
            </div>
        </div>
    </div>
</section>