<?php
    if (isset($_POST['btn_cari'])){
        $judul_buku = $_POST['judul_buku'];
        echo "<script>window.location='index.php?page=listdatabuku&judul_buku=$judul_buku'</script>";
    }
?> 

<section class="content">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <div class="row clearfix">
            <form action="" method="POST">
                <div class="col-sm-9">
                    <div class="form-group form-group-lg">
                        <div class="form-line">
                            <input type="text" class="form-control" name="judul_buku" placeholder="NAMA BUKU" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <button type="submit" name="btn_cari" value="1" class="btn btn-primary waves-effect">CARI</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- Basic Examples -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    PENGARANG
                </h2>
                
            </div>
            <div class="body">
                <ul class="list-group">
                <?php
                    $r = $con->query("SELECT tb_buku.id_pengarang, tb_pengarang.nama_pengarang, COUNT(tb_buku.id_buku) AS total FROM tb_pengarang INNER JOIN tb_buku ON tb_pengarang.id_pengarang = tb_buku.id_pengarang GROUP BY tb_buku.id_pengarang");
                    while ($rr = $r->fetch_array()) {
                ?>
                    <a href="index.php?page=listdatabuku&id_pengarang=<?php echo $rr['id_pengarang'];?>" type="button" class="list-group-item"><?php echo $rr['nama_pengarang'];?> <span class="badge bg-blue"><?php echo $rr['total'];?></span></a>
                    <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- #END# Basic Examples -->

    <!-- Basic Examples -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    PENERBIT
                </h2>
                
            </div>
            <div class="body">
                <ul class="list-group">
                <?php
                    $s = $con->query("SELECT tb_buku.id_penerbit, tb_penerbit.nama_penerbit, COUNT(tb_buku.id_penerbit) AS total FROM tb_penerbit INNER JOIN tb_buku ON tb_penerbit.id_penerbit = tb_buku.id_penerbit GROUP BY tb_buku.id_penerbit");
                    while ($ss = $s->fetch_array()) {
                ?>
                    <a href="index.php?page=listdatabuku&id_penerbit=<?php echo $ss['id_penerbit'];?>" type="button" class="list-group-item"><?php echo $ss['nama_penerbit'];?> <span class="badge bg-blue"><?php echo $ss['total'];?></span></a>
                    <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- #END# Basic Examples -->

    <!-- Basic Examples -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    KATEGORI
                </h2>
                
            </div>
            <div class="body">
                <ul class="list-group">
                <?php
                    $q = $con->query("SELECT tb_buku.id_kategori, tb_kategori.nama_kategori, COUNT(tb_buku.id_penerbit) AS total FROM tb_kategori INNER JOIN tb_buku ON tb_kategori.id_kategori = tb_buku.id_kategori GROUP BY tb_buku.id_kategori");
                    while ($qq = $q->fetch_array()) {
                ?>
                    <a href="index.php?page=listdatabuku&id_kategori=<?php echo $qq['id_kategori'];?>" type="button" class="list-group-item"><?php echo $qq['nama_kategori'];?> <span class="badge bg-blue"><?php echo $qq['total'];?></span></a>
                    <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- #END# Basic Examples -->
</section>