<!-- User Info -->
<div class="user-info">
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ucfirst($_SESSION['fullname']);?></div>
        <div class="email">You're Member</div>
    </div>
</div>
<!-- #User Info -->

<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li>
            <a href="index.php">
                <i class="material-icons col-red">cached</i>
                <span>List Buku yang di Pinjam</span>
            </a>
        </li>
        <li>
            <a href="index.php?page=databuku">
                <i class="material-icons col-red">book</i>
                <span>Peminjaman Buku</span>
            </a>
        </li>
        <li>
            <a href="logout.php">
                <i class="material-icons col-red">eject</i>
                <span>Logout</span>
            </a>
        </li>
    </ul>
</div>
<!-- #Menu -->